# Activitat 11
Escriu un programa que faça la sumatòria dels nombres enters compresos entre l'1 i el 10, és a dir, 1 + 2 + 3 + .... + 10.

```java
Algoritmo SUMATORIO
variables
    contador
    suma
INICIO
    contador = 1
    suma = 0
    MIENTRAS contador <= 10
        suma = contador + suma
        contador++
    FIN MIENTRAS
    Escribir "La suma es" + suma
FIN
```