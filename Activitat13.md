# Activitat 13
Dissenya un algorisme que demane 10 números a l'usuari, i per a cada número indique si és parell o imparell.

```java
	Algorisme ParellsImparells
	variables	
		numero enter
		i enter
	inici
		i ← 0
		mentre (i<10) fer
			Escriure 'Inserta número: '
			Llegir numero
			si (numero % 2 = 0)
				Escriure numero + 'és parell'
			sino
				Escriue numero + 'és imparell' 
			fi_si
			i ← i + 1
		fi_mentre
	fi
```