# Activitat 4
Dissenya un algorisme que visualitze en pantalla quants diners li donarà el banc a un client després de 6 mesos 
si posa 2000€ en un compte que li dona el 2,75% d'interès anual. Recorda que al pagar-te els interessos, el banc
aplicarà una retenció del 18% per a hisenda.

```java
Algorisme CalculaInteressos
variables
  depòsit
  interessos
  retenció
  totalGanado
INICI
  depòsit ← 2000
  interessos <- (depòsit * 0.0275) /2
  retenció <- interessos * 0.18) 
  totalGanado <- interessos - retenció
  Escriure 'El total de interesos ganados es: ' + totalGanado
FI
```
![A4-Diagrama de Flujo](images/PROG-UD1-A4-DF.png)