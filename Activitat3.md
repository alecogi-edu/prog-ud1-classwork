# Activitat 3
Dissenya un algorisme que mostre quant valdran unes deportives amb un preu de 85.00 euros, si estan rebaixades un 15%.

```java
Algorisme PreuDeportives
variables
  preuDep
  rebaixa 
  preuFinal 
INICI
  preuDep ← 85.00
  rebaixa ← 0.15
  preuFinal ← preuDep – preuDep*rebaixa
  Escriure 'El preu rebaixat es: ' + preuFinal
FI
```
![A3-Diagrama de Flujo](images/PROG-UD1-A3-DF.png)