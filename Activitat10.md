# Activitat 10
Dissenya un algoritme que demane tres valors els guarde en variables i mostre quin és el major i el més xicotet. 
Recorda veure que els tres valors siguen diferentes. En eixe cas haurà d'informar al usuari.
```java
Algorisme MajorMenor
variables
    num1
    num2
    num3
    major
    menor
inici
    Escriure 'Inserta el primer número'
    Llegir num1
    Escriure 'Inserta el segon número'
    Llegir num2
    Escriure 'Inserta el tercer número'
    Llegir num3
    SI num1 != num2
        SI num2 != num3
            SI num1 != num3
                major = num1
                SI num2 > major
                    major = num2
                FIN SI
                SI num3 > major
                    major = num3
                FIN SI
                menor = num1
                SI num2 < menor
                    menor = num2
                FIN SI 
                SI num3 < menor
                    menor = num3
                FIN SI 
                Escriure 'El major es ' + major + 'El menor es' + menor
                EIXIR
            FI SI
        FI SI
    FI SI
    Escriure "Els numeros han de ser distints"
FI
```