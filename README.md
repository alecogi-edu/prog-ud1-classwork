## Exercicis Unitat Didàctica 1.
#### Introducció a la programació. Algorismes i programes

[**Activitat1.-**](Activitat1.md) Dissenya un algorisme (diagrama de fluxe i pseudocodi) que permeta llegir les dades de un usuari (nom i cognoms) y els mostre 
per la pantalla.

[**Activitat2.-**](Activitat2.md)  Dissenya un algorisme (diagrama de fluxe i pseudocodi) que calcule el àrea de un quadrat i el mostre per pantalla

[**Activitat3.-**](Activitat3.md)  Dissenya un algorisme (diagrama de fluxe i pseudocodi) que mostre quant valdran unes deportives amb un preu de 85.00 euros, si estan rebaixades un 15%.

[**Activitat4.-**](Activitat4.md) Dissenya un algorisme (diagrama de fluxe i pseudocodi) que visualitze en pantalla quants diners li donarà el banc a un client
després de 6 mesos si posa 2000€ en un compte que li dona el 2,75% d'interès anual. Recorda que al pagar-te els interessos,
el banc aplicarà una retenció del 18% per a hisenda.

[**Activitat5.-**](Activitat5.md) Dissenya un algorisme (diagrama de fluxe i pseudocodi) que demane a l'usuari dos números, els guarde en dos variables i intercanvie 
els valors. Ha de mostrar els valors intercanviats.  

[**Activitat6.-**](Activitat6.md) Una temperatura expressada en graus Celsius (el que coneixem com graus centígrads)
 pot ser convertida a una temperatura equivalent F (graus Fahrenheit) d'acord a la següent fórmula:
                                  f=(9/5)c+32
                        on f representa els graus Fahrenheit i c els Celsius.
Dissenya un algorisme (diagrama de fluxe i pseudocodi) que demane a l'usuari el número de graus Celsius i indique l'equivalent en Fahrenheit.

[**Activitat7.-**](Activitat7.md) Dissenya un algorisme (diagrama de fluxe i pseudocodi) que demane a l'usuari 3 números. Si el primer és positiu,
 ha de calcular el producte dels altres dos. En cas contrari, ha de calcular la suma. Mostrar en els dos casos el resultat.  
 (diagrama de fluxe i pseudocodi)

[**Activitat8.-**](Activitat8.md) Dissenya un algoritme (diagrama de fluxe i pseudocodi) que pregunte al usuari un número del 1 al 10 i imprimixca per pantalla la taula de multiplicar corresponent

[**Activitat9.-**](Activitat9.md) Dissenya un algoritme (diagrama de fluxe i pseudocodi) que visualitze una sèrie de números introduïts per teclat. 
El programa finalitzarà quan l'usuari introduïsca un -1. (diagrama de fluxe i pseudocodi)

[**Activitat10.-**](Activitat10.md) Dissenya un algoritme que demane tres valors els guarde en variables i ens mostre quin és el major i el més xicotet. 
Recorda veure que els tres valors siguen diferentes. En eixe cas haurà d'informar al usuari.

[**Activitat11.-**](Activitat11.md) Escriu un programa que faça la sumatòria dels nombres enters compresos entre l'1 i el 10, és a dir, 1 + 2 + 3 + .... + 10.

[**Activitat12.-**](Activitat12.md) Escriu un programa que sol·licite a l'usuari una quantitat de segons i la convertisca en dies, hores, minuts i segons. Visualitza el resultat per pantalla.

[**Activitat13.-**](Activitat13.md) Dissenya un algorisme que demane 10 números a l'usuari, i per a cada número indique si és parell o imparell.

[**Activitat14.-**](Activitat14.md) Dissenya(algorisme) d'un programa que determine si un any és de traspàs (bisiesto en castellà).
Un any és de traspàs si és múltiple de 4 (per exemple, 1984). No obstant això, els anys múltiples de 100 sols són de traspàs quan també són al mateix temps múltiples de 400 (per exemple, 1800 no és de traspàs, però 2000 sí).

[**Activitat15.-**](Activitat15.md) Dissenya un algorisme que demane 20 números a l'usuari, i mostre al final la quantitat de parells i la quantitat d'imparells que s'han introduït.