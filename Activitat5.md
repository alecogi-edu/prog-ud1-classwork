# Activitat 5 
Dissenya un algorisme que demane a l'usuari dos números, els guarde en dos variables i intercanvie 
els valors. Ha de mostrar els valors intercanviats.

```java
Algorisme IntercanviaValors
variables
    num1
    num2
    aux
inici
    Escriure 'Inserta número1: '
    Llegir num1
    Escriure 'Inserta número 2: ' 
    Llegir num2
    aux ← num1
    num1 ← num2
    num2 ← aux
    Escriure num1, num2
FI
```
![A5-Diagrama de Flujo](images/PROG-UD1-A5-DF.png)