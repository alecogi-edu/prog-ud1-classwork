# Activitat 7
Dissenya un algorisme que demane a l'usuari 3 números. Si el primer és positiu, ha de calcular el producte dels altres dos. 
En cas contrari, ha de calcular la suma. Mostrar en els dos casos el resultat.  (diagrama de fluxe i pseudocodi)
```java
    Algorisme SumaProducte
	variables
		num1
		num2
        num3
        resultat	
	INICI
		Escriure 'Inserta 3 números'		
		Llegir num1
        Llegir num2
        Llegir num3
        SI (num1 < 0)
            resultat = num2 + num3
        SI NO
            resultat = num2 * num3
        FIN SI
		Escriure 'El resultat es: ' + resultat
	FI
```
![A7-Diagrama de Flujo](images/PROG-UD1-A7-DF.png)