# Activitat 15
Dissenya un algorisme que demane 20 números a l'usuari, i mostre al final la quantitat de parells i la quantitat
d'imparells que s'han introduït.

```java
Algorisme ComptarParellsImparells
	VARIABLES
		numero enter
		i enter
		quantsParells enter
		quantsImparells enter	
	INICI
		i ← 0
		quantsParells ← 0
		quantsImparells ← 0
		MENTRE (i < 20) FER
		    Escriure 'Inserta número: '
			Llegir numero
			SI (numero mod 2 = 0)
				quantsParells ← quantsParells + 1
			SI NO
				quantsImparells ← quantsImparells + 1
			FI SI
				i ← i + 1
		FI MENTRE
			Escriure ‘Parells:’ , quantsParells, ‘Imparells: ‘, quantsImparells 
	FI
```