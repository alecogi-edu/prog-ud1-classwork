# Activitat 12
Escriu un programa que sol·licite a l'usuari una quantitat de segons i la convertisca en dies, hores, minuts i segons. 
Visualitza el resultat per pantalla.

```java
Algorisme Temps
	variables
		tempsInicial enter
		numDies enter
		numHores enter
		numMinuts enter	
		numSegons enter
		temps enter	//per guardar càlculs intermedis
	INICI
		numDies ← temps/86400
		temps ← temps mod 86400
		numHores ← temps/3600
		temps ← temps mod 3600
		numMinuts ← temps/60
		numSegons ← temps mod 60
		Escriure tempsInicial + 'són' + numDies + 'dies' + numHores + 'hores' + numMinuts + 'minuts i ' + numSsegons + 'segons'
	FI
```