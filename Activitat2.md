# Activitat 2
Dissenya un algorisme que calcule el àrea de un quadrat i el mostre per pantalla

```java
Algorisme CalculaArea
variables
  costat
INICI
  Escriure 'Introdueix costat '
  Llegir costat
  area ← costat * costat
  Escriure 'El àrea es: ' + area
FI
```
![A3-Diagrama de Flujo](images/PROG-UD1-A2-DF.png)