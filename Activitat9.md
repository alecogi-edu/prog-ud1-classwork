# Activitat 9
Dissenya un algoritme que visualitze una sèrie de números introduïts per teclat. 
El programa finalitzarà quan l'usuari introduïsca un -1. (diagrama de fluxe i pseudocodi)

```java
Algorisme INTRODUIR_NUMEROS
variables
    numero
INICI
    numero = 0
    MENTRE (numero != 1) 
        Escriure "Introdueix un número";
        LLegir numero;
        SI (numero != 1)
            Escriure numero;
        FIN SI
    FIN MENTRE    
FI
```
![A9-Diagrama de Flujo](images/PROG-UD1-A9-DF.png)