# Activitat 14
Dissenya(algorisme) d'un programa que determine si un any és de traspàs (bisiesto en castellà). Un any és de traspàs si és múltiple de 4 (per exemple, 1984).
No obstant això, els anys múltiples de 100 sols són de traspàs quan també són al mateix temps múltiples de 400 (per exemple, 1800 no és de traspàs, però 2000 sí).

```java
Algorisme EsAnyTraspas
	VARIABLES
	    any enter
	INICI
		SI (any % 4 == 0) FER
            SI (any % 100 == 0) FER
                SI (any % 400 != 0) FER
                    Escriure "El any és de traspas"
                    acabar; // finalitza el programa
                FIN SI
            FIN SI
            Escriure "El any és de traspas"
        SI NO
            Escriure "El any no es de traspas"
        FIN SI 
	FI
```