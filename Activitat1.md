# Activitat 1 
Dissenya un algorisme que permeta llegir les dades d'un usuari (nom i cognoms) y els mostre per pantalla

```java
Algorisme LLegirDades 
variables
  nom
  cognom
inici
  Escriure 'Introduir nom'
  Llegir nom
  Escriure 'Introduir cognom'
  Llegir cognom
  Escriure 'El teu nom es' + nom: ' i el cognom' + cognom
fi
```
![A3-Diagrama de Flujo](images/PROG-UD1-A1-DF.png)