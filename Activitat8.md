# Activitat 8
Dissenya un algoritme que pregunte al usuari un número del 1 al 10 i imprimixca per pantalla la taula de multiplicar corresponent

```java
Algorisme TaulaMultiplicar
variables
    contador
    resultat
    numero
inici
    Escriure 'Inserta un número'
    Llegir numero
    SI numero < 10
        Escriure "Taula de multiplicar del numero" + numero
        Escriure "-------------------------------"
        contador = 0;
        MENTRES contador <= 10
            resultat = contador * numero;
            Escriure numero + "*" + contador + "=" + resultat
            contador ++;
        FI MENTRES
    SI NO
        Escriure "El numero introduït no es correcte"
    FI SI
    Escriure "-------------------------------"
FI
```
![A8-Diagrama de Flujo](images/PROG-UD1-A8-DF.png)