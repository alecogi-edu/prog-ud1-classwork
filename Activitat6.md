# Activitat 6 
Una temperatura expressada en graus Celsius (el que coneixem com graus centígrads) pot ser convertida a una temperatura 
equivalent F (graus Fahrenheit) d'acord a la següent fórmula:
> f=(9/5)c+32
on f representa els graus Fahrenheit i c els Celsius.
Dissenya un algorisme que demane a l'usuari el número de graus Celsius i indique l'equivalent en Fahrenheit.

```java
Algorisme ConversioGraus
	variables
		grausC decimal
		grausF decimal	
	INICI
		Escriure 'Inserta graus Celsius: '		
		Llegir grausC
		grausF ← (9/5)*grausC + 32
		Escriure 'Graus Fahrenheit: ' + grausF
	FI
```
![A6-Diagrama de Flujo](images/PROG-UD1-A6-DF.png)